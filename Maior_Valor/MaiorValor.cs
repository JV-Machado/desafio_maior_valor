﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Maior_Valor
{
    class MaiorValor
    {
        public int[] Vect { get; set; }

        public MaiorValor()
        {
            Vect = new int[] { 5, 10 };
        }
        public void ObterMaiorValor()
        {         
            int i = 0;

            while (Vect[i] > Vect[i + 1])
            {
                Console.WriteLine(Vect[i]);
                break;
            }

            while (Vect[i] < Vect[i + 1])
            {
                Console.WriteLine(Vect[i + 1]);
                break;
            }

            while (Vect[i] == Vect[i + 1])
            {
                Console.WriteLine("Iguais");
                break;
            }
        }
    }
}
